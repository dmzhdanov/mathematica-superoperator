### Overview
Collection of functions to deal with a quantum-mechanical superoperator representation.
### Reference
see file Reference.pdf or wiki page
https://bitbucket.org/dm_zhdanov/mathematica-superoperator/wiki/Home
